# Aula 11 - Disciplina Introdução a BigData. 
## Objetivo
Correção da P1 e introdução a análise automática de dados

## Desafio! 

Usar os exemplo para classificar texto.

## Nosso problema:

* Capture 20 notícias sobre a cultura da soja
* Capture 20 notícias sobre a cultura da algodão
* Capture 20 notícias sobre a cultura da milho
* Capture 20 notícias sobre a cultura da outros

* Separe 5 nóticias de cada categoria para treinamento
* classifique o restante e indique o total de acertos

## Perguntas:

- O que é Naives bayes?

- O que é processamento de linguagem natural?


## Ambiente de Desenvolvimento

Ser possível executar um código em PHP.
Exemplo: `php captura.php` no Terminal.

a) acesso a internet.


## Recursos

Computadores com Linux.

### Como preparar o ambiente com PHP

Instalando o PHP no Ubuntu.

`sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt`

[Outro link para linux] (https://www.digitalocean.com/community/tutorials/como-instalar-a-pilha-linux-apache-mysql-php-lamp-no-ubuntu-14-04-pt)

[para windows](http://pt.wikihow.com/Instalar-o-XAMPP-para-Windows)

g)


### Como clonar o Projeto

`git clone git@gitlab.com:nallaworks/Ibigdata_Aula11.git`

## Entrega
Pessoal a entrega será: 

* Qual a taxa de acerto do seu classificador automática?
* Onde mais posso aplicar essa técnica?

Grato, 
Allan