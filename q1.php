
<?php
include './autoload.php'; //script para carregar a biblioteca de Machine Learning


$tokenizer = new HybridLogic\Classifier\Basic;
$classifier = new HybridLogic\Classifier($tokenizer);


$classifier->train('Certo', 'Se levarmos ao pé da letra, refere-se a uma grande quantidade de dados, porém como tenho aprendido no curso a ideia do Big Data vai além desse conceito.
O Big Data entendo que seja você ir além das análises de BI que seria: coletar determinados dados, organiza-los de forma estruturada, analisa-los conforme nossa necessidade e/ou interesse, gerar relatórios para tomadas de decisões em empresas e monitora-las. Pode-se dizer que até esse ponto o Big Data é igual, porém entendo que ele hoje está mais ligado ao Big Data Analytics que seria algo a mais como: analise em tempo real de informações, cruzamento de diversos tipos de informações para criar insights sobre produtos, tendências de mercado, valorização de minerais e afins.
Para o termo Big Data considera-se 5Vs, são eles: Volume, Velocidade, Variedade, Veracidade e Valor, sendo que em alguns casos os dois últimos não são considerados partes do Big Data.');
$classifier->train('Certo', 'BigData é a transformação dos dados coletados por máquinas, sensores ou seres humanos em conhecimento. É todo o armazenamento de dados que poderá ser analisado e transformado em conhecimento específico sobre uma área desejada. O termo "Big" refere-se a grande quantidade de dados gerados pelos equipamentos com o avanço da tecnologia de hoje em dia, e que se devidamente estudados e processados, ajudará nas tomadas de decisões desejadas. ');
//$classifier->train('Certo', 'This tea is hot!');

$classifier->train('Meio', 'Big Data é um curso que visa o futuro, tratando e analisando grandes dados com máxima velocidade e precisão para melhor eficacia em diversas áreas da industria e outro, trazendo facilidades para empregados e empregadores. ');
$classifier->train('Meio', 'BigData é a área técnica que trabalha com grande quantidade de dados, mas vale destacar que umas das principais funções de um Técnico em BigData é coo-relacionar dados relevantes afim de vender informações estratégicas para seu cliente, ou seja, basicamente em uma grande quantidade de dados o Técnico deve ser capaz de filtrar aquilo que é relevante para o cliente e de forma estratégica vender para ele aumentado a produtividade de seu cliente como beneficio ou ate mesmo solucionar algum problema.');
$classifier->train('Meio', 'é um modelo de tratamento , disponibilização e utilização de dados com proposito e interação ao mundo exterior tratando com capacidade de uma quantidade enorme de informação');

$classifier->train('Errado', 'O BigData é uma formar de analise de muitos dados criando uma perspectiva do que ira acontecer ou do que a pessoa gosta.');
$classifier->train('Errado', 'E a ciência que explica o grande volume de dados, kabalisticos, geométricos, ornamentais que servem de estrutura para criação do universo e das diversas formas de vida, materiais e imateriais que compõem a existência infinita de todos os seres.');
$classifier->train('Errado', 'Transformar dados em soluções');

echo "O que é Big Data? \n";
// abrir arquivo csv em modo de leitura
$res = fopen('respostas.csv', "r");
// obter os dados em cada linha
while (($data = fgetcsv($res, 100000,";")) !== FALSE) {

    $texto = "\"".$data[1]."\"";
    $groups = $classifier->classify($texto);
    echo $data[0]."| Certo = ".number_format($groups['Certo']*100,2)." | Meio = ".number_format($groups['Meio']*100,2)." | Errado = ".number_format($groups['Errado']*100,2)."\n";
    //var_dump($groups);
}
// fechar o fecha csv
fclose($res);
